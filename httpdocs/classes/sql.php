<?php

	class SQL {
		
		
		private $host;
		private $user;
		private $pass;
		private $db;
		private $port;
		private $socket;
		
		private $link;


		public function __construct($host, $user, $pass, $db, $port = False, $socket = False) {
			$this->host = $host;
			$this->user = $user;
			$this->pass = $pass;
			$this->db = $db;
			$this->port = $port;
			$this->socket = $socket;
			return $this->_connect();
		}
		
		
				private function _connect() {
					$this->link = mysqli_connect($this->host, $this->user , $this->pass, $this->db, $this->port, $this->socket);
					if ($this->link) {
						$this->query("SET NAMES UTF8");
						return True;
					}
					die('Błąd połączenia z Bazą danych!');
				}
				
				
		public function query($q) {
			return mysqli_query($this->link, $q);
		}
				
				
		public function select($table, $vars = False, $fields = False) {

			$query = "SELECT ";
			
			if ($fields) {
				foreach ($fields as $field) {
					if ($field == end($fields)) {
						$query .= "{$field}";
					}
					else {
						$query .= "{$field}, ";
					}
				}
			}
			else {
				$query .= "*";
			}
			
			$query .= " FROM {$table}";
			
			if ($vars) {
				$query .= " WHERE ";
				if (gettype($vars) == 'string') {
					$query .= $vars;
				}
				else if (gettype($vars) == 'array') {
					foreach ($vars as $var => $val) {
						if ($var == end(array_keys($vars))) {
							$query .= "{$var} = '{$val}'";
						}
						else {
							$query .= "{$var} = '{$val}' AND ";
						}
					}
				}
			}
//            echo $query;
			return $this->query($query);
			

		}
				
		
		
	}
